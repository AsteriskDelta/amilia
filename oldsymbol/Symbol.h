/* 
 * File:   Symbol.h
 * Author: Galen Swain
 *
 * Created on September 5, 2015, 10:50 AM
 */

#ifndef SYMBOL_H
#define	SYMBOL_H
#include "AmiliaInt.h"
#include "SymbolLinkTable.h"

class Symbol {
public:
    Symbol();
    Symbol(const Symbol& orig);
    virtual ~Symbol();
    
    SymbolLink represents;
    
    SymbolSigT significance;
    SymbolSigT activation, deltaActivation;
    //Time updateTime;
    
    SymbolLinkTable assoc;
    SymbolLinkTable expect;
    SymbolLinkTable children;
    SymbolLinkTable parents;
    
private:

};

#endif	/* SYMBOL_H */

