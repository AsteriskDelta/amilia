/* 
 * File:   SymbolLinkTable.h
 * Author: Galen Swain
 *
 * Created on September 5, 2015, 11:05 AM
 */

#ifndef SYMBOLLINKTABLE_H
#define	SYMBOLLINKTABLE_H
#include "SymbolLink.h"

class SymbolLinkTable {
public:
    SymbolLinkTable();
    SymbolLinkTable(const SymbolLinkTable& orig);
    virtual ~SymbolLinkTable();
private:

};

#endif	/* SYMBOLLINKTABLE_H */

