/* 
 * File:   SymbolLink.h
 * Author: Galen Swain
 *
 * Created on September 5, 2015, 11:03 AM
 */

#ifndef SYMBOLLINK_H
#define	SYMBOLLINK_H

class SymbolLink {
public:
    SymbolLink();
    SymbolLink(const SymbolLink& orig);
    virtual ~SymbolLink();
private:

};

#endif	/* SYMBOLLINK_H */

