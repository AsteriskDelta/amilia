CC_SYS_FLAGS =
LIB_DIR = lib/
OBJ_DIR = objects/
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ABS_DIR := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

BUILD_DIRS = emote intrin symbol lib
BUILD_DYN = dyn/libAMLIntrinAudio.so dyn/libAMLSymbol.so dyn/libAMLLib.so

DYN_LIB_FLAGS = -L./ -L./dyn/ -Wl,-R./ -Wl,-R./dyn/
DYN_LIB_ARGS = $(patsubst dyn/lib%.so, -l% , $(BUILD_DYN))

LIB_CLASSES = 
PCH =
OBJS += $(patsubst regulate%.cpp,objects/regulate/%.o,$(shell find regulate/ -type f -name '*.cpp'))
OBJS += $(patsubst emote/%.cpp,objects/emote/%.o,$(shell find emote/ -type f -name '*.cpp'))
OBJS += $(patsubst intrin/%.cpp,objects/intrin/%.o,$(shell find intrin/ -type f -name '*.cpp'))
OBJS += $(patsubst symbol/%.cpp,objects/symbol/%.o,$(shell find symbol/ -type f -name '*.cpp'))
OBJS += $(patsubst stream/%.cpp,objects/stream/%.o,$(shell find stream/ -type f -name '*.cpp'))
LIB_OBJS += $(patsubst lib/%.cpp,objects/lib/%.o,$(shell find lib/ -type f -name '*.cpp'))
#PCH += $(LIB_PCH:%.h=$(LIB_DIR)%.h.gch)
#OBJS += $(LIB_CLASSES:%.cpp=$(OBJ_DIR)$(LIB_DIR)%.o)

FC_LIBS += -lboost_filesystem -lboost_system
FC_LIBS += -Wl,-Bstatic -lboost_system -lboost_filesystem -Wl,-Bdynamic -lGL -lGLEW -lGLU `sdl2-config --libs` -L./dyn/ -lassimp -lfreetype

CC = g++ -std=c++0x -fms-extensions $(CC_SYS_FLAGS)
OLDCC = g++ $(CC_SYS_FLAGS)

OPTI = -mtune=native -march=native -O3
debug: CCPLUS = $(OPTI) -g -DDEBUG
release: CCPLUS = $(OPTI)  -DOPTIMIZE

CFLAGS = -Wl,--no-as-needed -pipe -c -Iengine/ -I$(LIB_DIR) -I./ $(CCPLUS) -MMD -MP
BFLAGS = -pipe -c $(CCPLUS)
CFLAGSNW = $(CFLAGS)
CFLAGS += -Wall -Wextra
LFLAGS = -Wall -Wextra -pipe -Iengine/ -I$(LIB_DIR) -I./ $(CCPLUS) -MMD -MP
PIC = -fPIC
#release: LFLAGS += -Lext/static-linux

LIBS += -Lengine/ -lEngine
debug:   LFLAGS +=
release: LFLAGS +=

debug : $(PCH) 
release : $(PCH) 

#dependent builds
.cpp.o: $(PCH)
	$(CC) $(CFLAGS) $< -o $@

$(OBJ_DIR)%.o: %.cpp %.h
	@mkdir -p "$(@D)"
	$(CC) $(CFLAGS) $(PIC) $< -o $@

debug: $(BUILD_DYN) amilia
	
release: $(BUILD_DYN) amilia
	
dynRIAud = $(addprefix objects/intrin/auditory/, PCMSource.o PCMSourcePulse.o SpectralSource.o)
dyn/libAMLIntrinAudio.so: $(dynRIAud)
	g++ -shared -Wl,-soname,libAMLIntrinAudio.so -o dyn/libAMLIntrinAudio.so $(dynRIAud)  $(LIBS)
	
dynRSymb = $(addprefix objects/symbol/, Symbol.o SymbolLink.o SymbolLinkTable.o)
dyn/libAMLSymbol.so: $(dynRSymb)
	g++ -shared -Wl,-soname,libAMLSymbol.so -o dyn/libAMLSymbol.so $(dynRSymb)  $(LIBS)
	
dyn/libAMLLib.so: $(LIB_OBJS) $(OBJS)
	g++ -shared -Wl,-soname,libAMLLib.so -o dyn/libAMLLib.so $(LIB_OBJS) $(OBJS)  $(LIBS) $(FC_LIBS)
	
amilia : bootstrap/main.cpp $(BUILD_DIRS) $(BUILD_DYN)
	$(CC) $(LFLAGS) $(DYN_LIB_FLAGS) $(DYN_LIB_ARGS) $< -o amilia $(LIBS) $(FC_LIBS)

#secondary instructions
clean:
	rm -r objects/*
	
install: 
	

uninstall:


-include $(OBJFILES:.o=.d)
.FORCE: