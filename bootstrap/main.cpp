#include "AmiliaInt.h"
#include "lib/Application.h"
#include "symbol/AllSymbol.h"
#include "stream/AllStream.h"
#include "intrin/Intrinsics.h"
#include "intrin/temporal/Time.h"

int main(int argc, char** argv) {
  _unused(argc); _unused(argv);
  try {
    Application::BeginInitialization();
    Application::SetDataDir("./");
    Application::SetName("Amilia");
    Application::SetCodename("amilia.core");
    Application::SetCompanyName("Delta III Tech");
    Application::SetVersion("0.0r1X");
    Application::SetDebug(true);
    Application::EndInitialization();

    std::cout << "Running:\n";
    
    SymbolAxes::Initialize();
    Intrinsics::Initialize();
    Intrinsics::Setup();
    
    SymbolAxis *testAxis = new SymbolAxis();
    
    SymbolAxes::AddAxis(testAxis);
    
    Stream *stream = new Stream();
    stream->initialize();
    
    Symbol *tSym = new Symbol();
    stream->memory.addSymbol(tSym);
    
    int iterInc = 0, totalInc = 0;
    while(true) {
        float iterVal = float(iterInc);
        SymbolInstance *newSym = new SymbolInstance(tSym);
        Intrinsics::newInstanceHook(newSym);
        newSym->position.setAxis(testAxis, iterVal);
        stream->addInstance(newSym);
        
        stream->process();
        
        if(iterInc == 5)  {
            totalInc++;
            stream->associations.debugTable();
        }
        iterInc = (iterInc+1)%6;
        Intrinsics::time->currentTime += 1.f;
        
        if(totalInc == 5) break;
    }
    
    stream->halt();
    delete stream;
    SymbolAxes::Cleanup();

    Application::Quit(0);
  } catch(Application::Exiting e) {
    return 0;
  } catch(std::exception e) {
    std::cout << "Exiting: " << e.what() << "\n";
    return 1;
  }
}

