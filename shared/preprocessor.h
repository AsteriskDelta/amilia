#pragma once

#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)
#define GCC_VERSION STR(__GNUC__) "." STR(__GNUC_MINOR__) "." STR(__GNUC_PATCHLEVEL__)