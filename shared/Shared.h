#ifndef INC_SHARED
#define INC_SHARED

#define DCDEBUG
//#pragma GCC diagnostic ignored "-Wnarrowing"

#ifdef OPTIMIZE
#define OPT_INLINE
#endif

#include "preprocessor.h"
#include "defines.h"
#include "types.h"
#include "functions.h"
#include "structs.h"

#endif