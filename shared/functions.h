#pragma once
#include <limits>

#ifndef FUNC_CLAMP
#define FUNC_CLAMP
template <class T>
inline float clamp(T x, T a, T b) {
  return x < a ? a : (x > b ? b : x);
}
#endif

#ifndef FUNC_SATS
#define FUNC_SATS
template<typename T> T satAdd(T first, T second) {
  return first > std::numeric_limits<T>::max() - second ? std::numeric_limits<T>::max() : first + second;
}
template<typename T> T satSub(T first, T second) {
  return second > first ? std::numeric_limits<T>::min() : first - second;
}
#endif

template <class T>
inline T extreme(T a, T b) {
  if(abs(a) > abs(b)) return a;
  else return b;
}

template <typename T>
inline T max(T a, T b) {
  return a > b ? a : b;
  //return std::max(a, b);
}

template <typename T>
inline T min(T a, T b) {
  return a < b ? a : b;
  //return std::min(a, b);
}

template <typename T> bool between(T v, T s, T b) {
  return (v > s && v < b);
}

template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

template<typename K>
inline K wrapMP(K in) {
  if(in >= (K)M_PI) return in - (K)M_PI;
  else if(in <= -(K)M_PI) return in + (K)M_PI;
  else return in;
}

template <class TL>
inline TL lerp(TL a, TL b, float m) {
  m = clamp(m, 0.0f, 1.0f);
  return a * (1.0f - m) + b * m;
}

// The result (the remainder) has same sign as the divisor.
// Similar to matlab's mod(); Not similar to fmod() -   Mod(-3,4)= 1   fmod(-3,4)= -3
template<typename T>
T sfmod(T x, T y) {
  //if (0.== y) return x;
  double m= x - y * floor(x/y);
  
  // handle boundary cases resulted from floating-point cut off:
  if (y > 0) {            // modulo range: [0..y)
    if (m >= y) return 0;// Mod(-1e-16             , 360.    ): m= 360.
    else if (m < 0) {
      if(y+m == y) return 0  ; // just in case...
      else return y+m; // Mod(106.81415022205296 , _TWO_PI ): m= -1.421e-14 
    }
  } else {                  // modulo range: (y..0]
    if (m<=y) return 0;// Mod(1e-16              , -360.   ): m= -360.
    
    if (m>0 ) {
      if (y+m == y) return 0  ; // just in case...
      else return y+m; // Mod(-106.81415022205296, -_TWO_PI): m= 1.421e-14 
    }
  }
  
  return m;
}

// wrap [rad] angle to [-PI..PI)
inline double wrapPi(double fAng) {
  return sfmod(fAng + _PI, _TWO_PI) - _PI;
}

inline float fWrapPi(float fAng) {
  return sfmod(fAng + (float)_PI, (float)_TWO_PI) - (float)_PI;
}

// wrap [rad] angle to [0..TWO_PI)
inline double wrapTwoPi(double fAng) {
  return sfmod(fAng, _TWO_PI);
}

// wrap [deg] angle to [-180..180)
inline double wrap180(double fAng) {
  return sfmod(fAng + 180., 360.) - 180.;
}

// wrap [deg] angle to [0..360)
inline double wrap360(double fAng) {
  return sfmod(fAng ,360.);
}

template<typename K>
inline K lerpAngle(K u, K v, K p) {
  return u + p*((K)sfmod((v-u) + (K)_PI, (K)_TWO_PI) - (K)_PI);
}

template<typename T>
inline void ensureTrailing(T &str, const char c) {
  if(str[str.length()-1] != c) str += c;
}

template<typename T>
inline void trimTrailing(T &str, const char c) {
  if(str[str.length()-1] == c) str = str.substr(0, str.length()-1);
}

template <typename T> inline T smear(T mask, T val) {
  T ret = (mask & val);
  if(ret != (T)(0x0)) return ~((T)0x0) & mask;
  else return (T)(0x0);
}
template <typename T> inline T excl(T mask, T ret) {
  ret &= mask; return ret ^= (ret & (ret-1));
}

inline uint32_t ROL32 (uint32_t x, uint32_t n) {
  return (x<<n) | (x>>(32-n));
}

inline uint64_t ROL64 (uint64_t x, uint64_t n) {
  return (x<<n) | (x>>(64-n));
}

#include <x86intrin.h>

inline uint64_t FSB64(uint64_t v) {
    /*uint64_t ret;
    asm("movl %1, %%rax;
	"movl %%rax, %0;"
		:"=r"(ret)
		:"r"(v)	
		:"%rax");
    return ret;*/
    return __lzcnt64(v);
}
inline uint64_t FSB32(uint64_t v) {
    /*uint64_t ret;
    asm("movl %1, %%rax;
	"movl %%rax, %0;"
		:"=r"(ret)
		:"r"(v)	
		:"%rax");
    return ret;*/
    return __lzcnt32(v);
}