#pragma once

#define ENGINE_NAME "ARKE"
#define ENGINE_V_P 0
#define ENGINE_V_S 0
#define ENGINE_V_R 1
#define ENGINE_INFOSTRING  (ENGINE_NAME " v" STR(ENGINE_V_P) "." STR(ENGINE_V_S) "-" STR(ENGINE_V_R) " built with G++ " GCC_VERSION

#include <stdint.h>
#include <cmath>
/*
#ifndef _WIN32
# include <unistd.h>
# include <utime.h>
#else
# include <direct.h>
# include <io.h>
#endif
*/
#ifndef _unused
#define _unused(x) ((void)x)
#endif

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80

#define BIT00 0x01
#define BIT01 0x02
#define BIT02 0x04
#define BIT03 0x08
#define BIT04 0x10
#define BIT05 0x20
#define BIT06 0x40
#define BIT07 0x80
#define BIT08 (0x1 << 8 )
#define BIT09 (0x1 << 9 )
#define BIT10 (0x1 << 10)
#define BIT11 (0x1 << 11)
#define BIT12 (0x1 << 12)
#define BIT13 (0x1 << 13)
#define BIT14 (0x1 << 14)
#define BIT15 (0x1 << 15)

#define BIT(b) (0x1 << (b))

#define UP	BIT0
#define DOWN	BIT1
#define LEFT	BIT2
#define RIGHT	BIT3
#define FRONT	BIT4
#define BACK	BIT5

#define RAD_TO_DEG (180.0f / (float)M_PI)
#define DEG_TO_RAD ((float)M_PI / 180.0f)

#define RAD_360 (2.0 * M_PI)
#define RAD_360F ((float)(2.0 * M_PI))

#ifdef __GNUC__
#define pure __attribute__((pure))
#define unroll __attribute__((optimize("unroll-loops"))) \
#define tls __thread
#define likely(x) (__builtin_expect(!!(x), 1))
#define unlikely(x) (__builtin_expect(!!(x), 1))
#else
#define pure [[pure]]
#endif

#define RDID() __COUNTER__

#define _default template<class=void>
#define _defaultDef template<class>

#ifdef __cplusplus
#define PCAST(type, pointer) reinterpret_cast<type>(pointer)
#else
#define PCAST(type, pointer) (type)(pointer)
#endif

template<typename T> inline T postIncrement(T& v, const T& inc) {
    const T temp = v;
    v = v + inc;
    return temp;
}

#ifdef UNITTEST
#define TEST_BEGIN  
#define TEST_END  
#else
#define TEST_BEGIN #if 0
#define TEST_END #endif
#endif

//define _isnan to avoid errors
#ifdef __INTEL_COMPILER
#include <cmath>
#include <cfloat>
#define _isnan std::isnan
#define	_FPCLASS_SNAN	0x0001	/* Signaling "Not a Number" */
#define	_FPCLASS_QNAN	0x0002	/* Quiet "Not a Number" */
#define	_FPCLASS_NINF	0x0004	/* Negative Infinity */
#define	_FPCLASS_NN	0x0008	/* Negative Normal */
#define	_FPCLASS_ND	0x0010	/* Negative Denormal */
#define	_FPCLASS_NZ	0x0020	/* Negative Zero */
#define	_FPCLASS_PZ	0x0040	/* Positive Zero */
#define	_FPCLASS_PD	0x0080	/* Positive Denormal */
#define	_FPCLASS_PN	0x0100	/* Positive Normal */
#define	_FPCLASS_PINF	0x0200	/* Positive Infinity */
#define _fpclass std::fpclassify
#endif

#ifdef _WIN64
  #define SYS_WINDOWS
#elif _WIN32
  #define SYS_WINDOWS
#elif __APPLE__
  #include "TargetConditionals.h"
  #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
    #define SYS_IPHONE
  #elif TARGET_OS_IPHONE
    #define SYS_IPHONE
  #else
    #define TARGET_OS_OSX 1
    #define SYS_MAC
  #endif
#elif __linux
  #define SYS_NIX
#elif __unix // all unices not caught above
  #define SYS_NIX
#elif __posix
  #define SYS_NIX
#endif

#if defined(SYS_NIX) || defined(SYS_MAC)
#define DIR_DELIM '/'
#elif defined(SYS_WINDOWS)
#define DIR_DELIM '\\'
#endif

#ifdef DEBUG
#define DO_DEBUG true
#else
#define DO_DEBUG false
#endif

#define RMASK 0x000000ff
#define GMASK 0x0000ff00
#define BMASK 0x00ff0000
#define AMASK 0xff000000

static const double     _PI= 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348;
static const double _TWO_PI= 6.2831853071795864769252867665590057683943387987502116419498891846156328125724179972560696;

class SerializableClass {
  virtual unsigned int serialLength() { return 0; };
  virtual void serialize(char *const buffer, int length = -1) { _unused(buffer); _unused(length); };
};

#define Serializable public SerializableClass

#ifndef NULL
#define NULL 0
#endif