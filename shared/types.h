#pragma once

typedef uint8_t			Uint8;
typedef unsigned short		Uint16;
typedef unsigned int		Uint32;
typedef uint64_t			Uint64;

typedef int8_t			Sint8;
typedef short int		Sint16;
typedef int			Sint32;
typedef int64_t			Sint64;

typedef int8_t			Int8;
typedef short int		Int16;
typedef int			Int32;
typedef int64_t			Int64;

typedef unsigned int		GlUint;
typedef int			GlInt;

typedef unsigned long long	PtrInt;

typedef uint8_t			U8;
typedef int8_t			S8;
typedef int8_t			I8;

typedef unsigned short		U16;
typedef short			S16;
typedef short			I16;

typedef unsigned int		U32;
typedef int			S32;
typedef int			I32;

typedef uint64_t			U64;
typedef int64_t			S64;
typedef int64_t			I64;

#define MAX_Uint8	256
#define MAX_Uint16	65535

#define MIN_Int16	-32768
#define MAX_Int16	32767