#pragma once

union Int2x3 {
  struct { 
    unsigned int a:2;
    unsigned int b:2;
    unsigned int c:2;
    bool d:1;
  };
  struct { 
    char x:2;
    char y:2;
    char z:2;
    bool w:1;
  };
  unsigned char raw;
  
  inline bool operator== (const Int2x3 m) const { return raw == m.raw; };
};

union Int4x2 {
  struct { 
    unsigned int a:4;
    unsigned int b:4;
  };
  struct { 
    char p:4;
    char m:4;
  };
  struct { 
    char s:4;
    char e:4;
  };
  unsigned char raw;
  
  inline bool operator== (const Int4x2 m) const { return raw == m.raw; };
};

union Int4x4 {
  struct { 
    unsigned int a:4;
    unsigned int b:4;
    unsigned int c:4;
    unsigned int d:4;
  };
  struct { 
    char x:4;
    char y:4;
    char z:4;
    char w:4;
  };
  unsigned short raw;
  
  inline bool operator== (const Int4x4 m) const { return raw == m.raw; };
};

union Int4x6 {
  struct { 
    unsigned int a:4;
    unsigned int b:4;
    unsigned int c:4;
    unsigned int d:4;
    unsigned int e:4;
    unsigned int f:4;
  };
  struct { 
    char x:4;
    char y:4;
    char z:4;
    char w:4;
    char i:4;
    char j:4;
  };
  unsigned int raw : 24;
  
  inline bool operator== (const Int4x6 m) const { return raw == m.raw; };
};
