/* 
 * File:   Time.h
 * Author: Galen Swain
 *
 * Created on September 12, 2015, 12:32 PM
 */

#ifndef TIME_H
#define	TIME_H
#include <AmiliaInt.h>
#define NS_PER_SECOND (1000*1000*1000)

class Time {
public:
    Time();
    Time(const Time& orig);
    virtual ~Time();
    
    Int64 timestamp;
    Int32 nsOffset;
    inline Time(Int64 nt, Int32 ns) : timestamp(nt), nsOffset(ns) {};
    
    inline operator double() const;
    inline bool operator<(const Time& o) const;
    inline bool operator>(const Time& o) const;
    
    inline Time operator-(const Time& o) const;
    inline Time operator+(const Time& o) const;
    
    static double getT(const Time& t, const Time& s, const Time& e);
private:
    static Int64 baseTimestamp;
    
    inline Int64 getBaseRel() const {
        return timestamp - baseTimestamp;
    }
};

inline Time::operator double() const {
    return double(this->getBaseRel()) + double(nsOffset)/double(NS_PER_SECOND);
}
inline bool Time::operator<(const Time& o) const {
    return this->timestamp < o.timestamp || (this->timestamp == o.timestamp && this->nsOffset < o.nsOffset);
}
inline bool Time::operator>(const Time& o) const {
    return this->timestamp > o.timestamp || (this->timestamp == o.timestamp && this->nsOffset > o.nsOffset);
}
inline Time Time::operator-(const Time& o) const {
    Time ret;
    ret.timestamp = timestamp - o.timestamp;
    if(nsOffset > o.nsOffset) ret.nsOffset = nsOffset - o.nsOffset;
    else ret.nsOffset = (nsOffset - o.nsOffset) + NS_PER_SECOND;
    return ret;
}
inline Time Time::operator+(const Time& o) const {
    Time ret;
    ret.timestamp = timestamp + o.timestamp;
    ret.nsOffset = nsOffset + o.nsOffset;
    if(ret.nsOffset > NS_PER_SECOND) {
        ret.nsOffset -= NS_PER_SECOND;
        ret.timestamp += 1;
    }
    return ret;
}

#endif	/* TIME_H */

