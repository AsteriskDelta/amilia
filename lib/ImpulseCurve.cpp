/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImpulseCurve.cpp
 * Author: Galen Swain
 * 
 * Created on April 3, 2016, 6:57 PM
 */

#include "ImpulseCurve.h"

ImpulseCurve::ImpulseCurve() {
}

ImpulseCurve::ImpulseCurve(const ImpulseCurve& orig) {
  center = orig.center;
  p1 = orig.p1; p2 = orig.p2;
  range1 = orig.range1; range2 = orig.range2;
}

ImpulseCurve::~ImpulseCurve() {
}

float ImpulseCurve::operator()(float t) const {
  float relT = (t - center);
  if(relT < 0.f) relT /= range1;
  else relT /= range2;
  
  if(fabs(relT) > 1.f) return 0.f;
  else return pow(sin(M_PI * relT),((relT > 0.f)? p2 : p1));
}