/* 
 * File:   Time.cpp
 * Author: Galen Swain
 * 
 * Created on September 12, 2015, 12:32 PM
 */

#include "Time.h"

Int64 Time::baseTimestamp = 0;
double Time::getT(const Time& t, const Time& s, const Time& e) {
  double tot = double(e - s);
  double del = double(t - s);
  return clamp(del/tot, 0.0, 1.0);
}

Time::Time() {
  timestamp = 0;
  nsOffset = 0;
}

Time::Time(const Time& orig) {
  timestamp = orig.timestamp;
  nsOffset = orig.nsOffset;
}

Time::~Time() {
}

