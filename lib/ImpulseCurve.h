/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ImpulseCurve.h
 * Author: Galen Swain
 *
 * Created on April 3, 2016, 6:57 PM
 */

#ifndef IMPULSECURVE_H
#define IMPULSECURVE_H
#include <AmiliaInt.h>

class ImpulseCurve {
public:
    ImpulseCurve();
    ImpulseCurve(const ImpulseCurve& orig);
    virtual ~ImpulseCurve();
    
    float operator()(float t) const;
    
    float center;
    float p1, p2;
    float range1, range2;
private:

};

#endif /* IMPULSECURVE_H */

