/* 
 * File:   AmiliaInt.h
 * Author: Galen Swain
 *
 * Created on September 7, 2015, 10:02 AM
 */

#ifndef AMILIAINT_H
#define	AMILIAINT_H

#include "shared/Shared.h"
#include <iostream>
#include <sstream>
#include <limits>

#include "lib/Flags.h"
#include "lib/Time.h"

typedef double SymbolSigT;
typedef float sigt;
typedef unsigned short HrIdx;
typedef double HrVal;

#define SOFTERR(x) std::cerr << x << std::endl

#endif	/* AMILIAINT_H */

