/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Hormone.h
 * Author: Galen Swain
 *
 * Created on April 3, 2016, 4:42 PM
 */

#ifndef HORMONE_H
#define HORMONE_H
#include "AmiliaInt.h"
#include <list>
#include <lib/Color.h>

#include "HormoneContrib.h"

class Hormone {
public:
    Hormone();
    Hormone(const Hormone& orig);
    virtual ~Hormone();
    
    inline operator HrVal() const;
    inline  HrVal dT() const;
    
    HrIdx idx;
    HrVal value, dvalueDt;
    
    std::list<HormoneContrib> contributes;
    
    void update();
    
    std::string dbgName;
    ColorRGBA dbgColor;
private:
    HrVal prevValue;
    Time prevTime;
};

inline Hormone::operator HrVal() const {
     return this->value;
}
inline HrVal Hormone::dT() const {
     return this->dvalueDt;
}

#endif /* HORMONE_H */

