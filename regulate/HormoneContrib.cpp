/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HormoneContrib.cpp
 * Author: Galen Swain
 * 
 * Created on April 3, 2016, 5:39 PM
 */

#include "HormoneContrib.h"

HormoneContrib::HormoneContrib() {
  isOneshot = false;
  value = 0;
}

HormoneContrib::HormoneContrib(const HormoneContrib& orig) {
  isOneshot = orig.isOneshot;
  curve = orig.curve;
  start = orig.start;
  end = orig.end;
  value = orig.value;
}

HormoneContrib::~HormoneContrib() {
}

double HormoneContrib::get(const Time& t) const {
  return value * curve(Time::getT(t, start, end));
}
bool HormoneContrib::isRemovable(const Time& t) const {
  return isOneshot && t > end;
}