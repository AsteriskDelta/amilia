/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HormoneContrib.h
 * Author: Galen Swain
 *
 * Created on April 3, 2016, 5:39 PM
 */

#ifndef HORMONECONTRIB_H
#define HORMONECONTRIB_H

#include "AmiliaInt.h"
#include <lib/Time.h>
#include <lib/ImpulseCurve.h>

class HormoneContrib {
public:
    HormoneContrib();
    HormoneContrib(const HormoneContrib& orig);
    virtual ~HormoneContrib();
    
    bool isOneshot;
    double value;
    Time start, end;
    ImpulseCurve curve;
    
    double get(const Time& t) const;
    bool isRemovable(const Time& t) const;
    
    inline double operator()(const Time& t) const { return this->get(t); };
private:

};

#endif /* HORMONECONTRIB_H */

