/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Association.h
 * Author: galen
 *
 * Created on September 9, 2016, 5:13 PM
 */

#ifndef ASSOCIATION_H
#define ASSOCIATION_H
#include "include.h"
#include "Memory.h"

class Stream;

class Association {
public:
    friend class Memory;
    friend class Stream;
    
    Association();
    Association(const Association& orig);
    virtual ~Association();
    
    Memory *memory;
    Stream *stream;
    
    void debugTable();
private:

};

#endif /* ASSOCIATION_H */

