/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Stream.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 4:54 PM
 */

#include "Stream.h"

Stream::Stream() {
}

Stream::Stream(const Stream& orig) {
}

Stream::~Stream() {
}

void Stream::initialize() {
    memory.stream = this;
    memory.associations = &associations;
    associations.memory = &memory;
    associations.stream = this;
}

void Stream::halt() {
    
}

void Stream::addInstance(SymbolInstance *instance) {
    this->associateInstance(instance);
    instances.insert(instance);
}

void Stream::removeInstance(SymbolInstance *instance) {
    instances.erase(instance);
}

void Stream::associateInstance(SymbolInstance *instance) {
    for(auto it = instances.begin(); it != instances.end(); ++it) {
        SymbolInstance *const otherInstance = *it;
        instance->associate(otherInstance);
    }
}

void Stream::process() {
    
}