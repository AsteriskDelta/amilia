/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Stream.h
 * Author: galen
 *
 * Created on September 9, 2016, 4:54 PM
 */

#ifndef STREAM_H
#define STREAM_H
#include "include.h"
#include "Memory.h"
#include "Association.h"
#include <set>

class SymbolInstance;

class Stream {
public:
    friend class Memory;
    friend class Association;
    
    Stream();
    Stream(const Stream& orig);
    virtual ~Stream();
    
    Association associations;
    Memory memory;
    
    std::set<SymbolInstance*> instances;
    
    void initialize();
    void halt();
    
    void addInstance(SymbolInstance *instance);
    void removeInstance(SymbolInstance *instance);
    void associateInstance(SymbolInstance *instance);
    
    void process();
private:

};

#endif /* STREAM_H */

