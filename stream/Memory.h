/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Memory.h
 * Author: galen
 *
 * Created on September 9, 2016, 5:14 PM
 */

#ifndef MEMORY_H
#define MEMORY_H
#include "include.h"
#include "symbol/AllSymbol.h"
#include <vector>

class Association;
class Stream;

class Memory {
public:
    friend class Association;
    friend class Stream;
    Memory();
    Memory(const Memory& orig);
    virtual ~Memory();
    
    Association *associations;
    Stream *stream;
    
    std::vector<SymbolPtr> symbols;
    
    void addSymbol(SymbolPtr &ptr);
private:

};

#endif /* MEMORY_H */

