/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolInstance.h
 * Author: galen
 *
 * Created on September 9, 2016, 4:44 PM
 */

#ifndef SYMBOLINSTANCE_H
#define SYMBOLINSTANCE_H
#include "include.h"
#include "SymbolPtr.h"
#include "SymbolPosition.h"

class SymbolInstance {
public:
    SymbolInstance(SymbolPtr &ss);
    SymbolInstance();
    SymbolInstance(const SymbolInstance& orig);
    virtual ~SymbolInstance();
    
    void setSymbol(SymbolPtr &ss);
    
    SymbolPtr sym;
    SymbolPosition position;
    sigt chance, strength;
    
    void associate(SymbolInstance *const o);
    
private:

};

#endif /* SYMBOLINSTANCE_H */

