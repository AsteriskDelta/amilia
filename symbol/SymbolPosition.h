/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolPosition.h
 * Author: galen
 *
 * Created on September 9, 2016, 4:50 PM
 */

#ifndef SYMBOLPOSITION_H
#define SYMBOLPOSITION_H
#include "include.h"
#include "AxisPtr.h"
#include <list>

#define SYMAXIS_NDEF (-std::numeric_limits<float>::infinity())

class SymbolPosition {
public:
    struct AxisValue {
        SymbolAxisPtr axis;
        union {
        float pos;
        double dpos;
        };
        inline AxisValue() : axis(nullptr), pos(0.f) {};
        inline AxisValue(SymbolAxisPtr ptr) : axis(ptr), pos(0.f) {};
    };
    SymbolPosition();
    SymbolPosition(const SymbolPosition& orig);
    virtual ~SymbolPosition();
    
    std::list<AxisValue> axes;
    
    void setAxis(SymbolAxisPtr axis, float v);
    float getAxis(SymbolAxisPtr axis) const;
    inline bool hasAxis(SymbolAxisPtr axis) const {
        return this->getRawAxisConst(axis) != nullptr;
    }
    
    SymbolPosition operator+(const SymbolPosition &o) const;
    SymbolPosition operator*(const SymbolPosition &o) const;
    SymbolPosition operator-() const;
    SymbolPosition operator-(const SymbolPosition &o) const;
    sigt magnitude() const;
protected:
    AxisValue* getRawAxis(SymbolAxisPtr axisPtr);
    const AxisValue* getRawAxisConst(SymbolAxisPtr axisPtr) const;
    AxisValue* addAxis(SymbolAxisPtr axisPtr);

};

#endif /* SYMBOLPOSITION_H */

