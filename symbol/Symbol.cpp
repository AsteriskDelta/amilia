/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Symbol.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 4:43 PM
 */

#include "Symbol.h"

Symbol::Symbol() {
}

Symbol::Symbol(const Symbol& orig) {
}

Symbol::~Symbol() {
}

void Symbol::associate(SymbolPtr other, const SymbolPosition &from, const SymbolPosition &to, sigt weight) {
    SymbolPosition deltaSelf, deltaOther;
    deltaSelf = to - from;
    deltaOther = from - to;
    this->associateAD(other, from, deltaSelf, weight);
    other->associateAD(this, to, deltaOther, weight);
}

void Symbol::associateAD(SymbolPtr other, const SymbolPosition &pos, const SymbolPosition &delta, sigt weight) {
    for(int oi = 0; oi <= 1; oi++) {
        SymbolPosition trans = (oi==0)?pos : delta;
        SymbolAssociationSet assocs = this->findAssociations(oi, trans, false);
        for(auto it = assocs.list.begin(); it != assocs.list.end(); ++it) {
            SymbolAssociation *const assoc = it->assoc;
            assoc->associate(other, trans, weight*it->w);
        }
    }
}

SymbolAssociationSet Symbol::findAssociations(int order, const SymbolPosition &pos, bool query) {
    SymbolAssociationSet ret; int orderID = 0;
    for(auto ot = orders.begin(); ot != orders.end(); ++ot) {
        unsigned int startSize = ret.list.size();
        
        for(auto it = ot->associations.begin(); it != ot->associations.end(); ++it) {
            float dist = it->getWeight(pos);
            if(dist < 0.6) ret.add(&(*it), dist);
        }
        if(ret.list.size() == startSize && !query) ret.add(this->addAssociation(orderID), 0.f);
        
        orderID++;
    }
    
    ret.process();
    return ret;
}

SymbolAssociation* Symbol::addAssociation(int order) {
    Order *orderPtr = this->getOrder(order);
    orderPtr->associations.push_back(SymbolAssociation());
    return &(orderPtr->associations.back());
}

Symbol::Order* Symbol::getOrder(int k) {
    return &orders[k];
}