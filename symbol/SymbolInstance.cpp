/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolInstance.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 4:44 PM
 */

#include "SymbolInstance.h"
#include "Symbol.h"

SymbolInstance::SymbolInstance(SymbolPtr& ss) : position() {
    this->setSymbol(ss);
}

SymbolInstance::SymbolInstance(): sym(nullptr), position() {
}

SymbolInstance::SymbolInstance(const SymbolInstance& orig) : sym(orig.sym), position(orig.position) {
}

SymbolInstance::~SymbolInstance() {
}

void SymbolInstance::setSymbol(SymbolPtr &ss) {
    sym = ss;
}

void SymbolInstance::associate(SymbolInstance *const o) {
    const float weight = (chance*strength) * (o->chance*o->strength);
    sym->associate(o->sym, position, o->position, weight);
    //Called by sym->associate to ensure reciprocity
    //o->sym->associate(sym, o->position, position, weight);
}