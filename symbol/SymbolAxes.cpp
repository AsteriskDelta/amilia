/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolAxes.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 4:50 PM
 */

#include "SymbolAxes.h"
#include <vector>

namespace SymbolAxes {
    std::vector<SymbolAxis*> axes;
    
    void Initialize() {
        
    }
    
    void AddAxis(SymbolAxis *axis) {
        axes.emplace_back(axis);
    }
    
    void Cleanup() {
        for(SymbolAxis * &axis : axes) delete axis;
        axes.clear();
    }
};
