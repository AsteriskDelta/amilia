/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolPosition.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 4:50 PM
 */

#include "SymbolPosition.h"

SymbolPosition::SymbolPosition() {
}

SymbolPosition::SymbolPosition(const SymbolPosition& orig) {
}

SymbolPosition::~SymbolPosition() {
}

void SymbolPosition::setAxis(SymbolAxisPtr axis, float v) {
    AxisValue* axisData = this->getRawAxis(axis);
    if(axisData == nullptr) axisData = this->addAxis(axis);
    
    if(axisData == nullptr) {
        SOFTERR("Couldn't add SymbolPosition AxisValue!");
        return;
    }
    axisData->pos = v;
}
float SymbolPosition::getAxis(SymbolAxisPtr axis) const {
    const AxisValue* axisData = this->getRawAxisConst(axis);
    if(axisData == nullptr) return SYMAXIS_NDEF;
    else return axisData->pos;
}

SymbolPosition::AxisValue* SymbolPosition::getRawAxis(SymbolAxisPtr axisPtr) {
    for(AxisValue &axis : axes) {
        if(axis.axis == axisPtr) return &axis;
    }
    return nullptr;
}
const SymbolPosition::AxisValue* SymbolPosition::getRawAxisConst(SymbolAxisPtr axisPtr) const{
    for(const AxisValue &axis : axes) {
        if(axis.axis == axisPtr) return &axis;
    }
    return nullptr;
}

SymbolPosition::AxisValue* SymbolPosition::addAxis(SymbolAxisPtr axisPtr) {
    axes.push_back(AxisValue(axisPtr));
    return &(axes.back());
}

SymbolPosition SymbolPosition::operator+(const SymbolPosition &o) const {
    SymbolPosition ret;
    //Add all axes together, from both sources (may contain different coord systems))
    for(const AxisValue& av : axes) {
        if(o.hasAxis(av.axis)) ret.setAxis(av.axis, av.pos + o.getAxis(av.axis));
        else ret.setAxis(av.axis, av.pos);
    }
    for(const AxisValue& av : o.axes) {
        if(this->hasAxis(av.axis)) continue;
        
        ret.setAxis(av.axis, av.pos);
    }
    return ret;
}
SymbolPosition SymbolPosition::operator*(const SymbolPosition &o) const {
    SymbolPosition ret;
    //Multiply all axes, leave those without a o[x] as they were
    for(const AxisValue& av : axes) {
        if(o.hasAxis(av.axis)) ret.setAxis(av.axis, av.pos * o.getAxis(av.axis));
        else ret.setAxis(av.axis, av.pos);
    }
    return ret;
}

SymbolPosition SymbolPosition::operator-() const {
    SymbolPosition ret;
    for(const AxisValue& av : axes) ret.setAxis(av.axis, -av.pos);
    return ret;
}
SymbolPosition SymbolPosition::operator-(const SymbolPosition &o) const {
    return *this + (-o);
}

sigt SymbolPosition::magnitude() const {
    sigt value = 0.f, base = 0.f;
    for(const AxisValue& av : axes) {
        value += av.pos;
        base += 1.f;
    }
    
    if(base == 0.f) return 0.f;
    else return pow(value, 1.f/base);
}