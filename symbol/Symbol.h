/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Symbol.h
 * Author: galen
 *
 * Created on September 9, 2016, 4:43 PM
 */

#ifndef SYMBOL_H
#define SYMBOL_H
#include "include.h"
#include "SymbolPtr.h"
#include "SymbolPosition.h"
#include "SymbolAssociation.h"
#include "SymbolImplication.h"
#include <list>
#include <vector>

class Symbol {
public:
    Symbol();
    Symbol(const Symbol& orig);
    virtual ~Symbol();
    
    struct Order {
        std::list<SymbolAssociation> associations;
    };
    std::vector<Order> orders;
    
    void associate(SymbolPtr other, const SymbolPosition &from, const SymbolPosition &to, sigt weight);
    void associateAD(SymbolPtr other, const SymbolPosition &pos, const SymbolPosition &delta, sigt weight);
    
    SymbolAssociationSet findAssociations(int order, const SymbolPosition &pos, bool query = true);
    SymbolAssociation *addAssociation(int order);
protected:
    Order* getOrder(int k);
};

#endif /* SYMBOL_H */

