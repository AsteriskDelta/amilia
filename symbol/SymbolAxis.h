/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolAxis.h
 * Author: galen
 *
 * Created on September 9, 2016, 4:45 PM
 */

#ifndef SYMBOLAXIS_H
#define SYMBOLAXIS_H
#include "include.h"

class SymbolAxis {
public:
    SymbolAxis();
    SymbolAxis(const SymbolAxis& orig);
    virtual ~SymbolAxis();
    
    int assocOrderMin, assocOrderMax;
protected:

};

#endif /* SYMBOLAXIS_H */

