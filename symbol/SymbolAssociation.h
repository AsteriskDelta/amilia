/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolAssoc.h
 * Author: galen
 *
 * Created on September 9, 2016, 8:02 PM
 */

#ifndef SYMBOLASSOC_H
#define SYMBOLASSOC_H
#include "include.h"
#include "SymbolAxis.h"
#include "SymbolPosition.h"
#include "SymbolImplication.h"
#include <list>

//Stored for each order of associations (absolute, delta, etc.)
class SymbolAssociation {
public:
    SymbolAssociation();
    SymbolAssociation(const SymbolAssociation& orig);
    virtual ~SymbolAssociation();
    
    //Hyperblob (super technical term, I know) center and radii in intrinsic dimensions)
    SymbolPosition center;
    SymbolPosition radiiWeights;
    
    std::list<SymbolImplication> implications;
    
    float getWeight(const SymbolPosition& p) const;
    void associate(SymbolPtr sym, const SymbolPosition &trans, sigt weight);
protected:

};

struct SymbolAssociationSet {
  struct Listing {
      SymbolAssociation *assoc;
      float w, dist;
  };
  std::list<Listing> list;
  float totalDistance;
  inline void add(SymbolAssociation *assoc, float d) {
      list.push_back(Listing{assoc, 0.f, d});
  }
  inline void process() {
      totalDistance = 0.f;
      for(Listing& l : list) {
          totalDistance += l.dist;
      }
      for(Listing& l : list) {
          l.w = 1.f - totalDistance / l.dist;
      }
  }
};

#endif /* SYMBOLASSOC_H */

