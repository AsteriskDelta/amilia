/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolImplication.h
 * Author: galen
 *
 * Created on September 9, 2016, 8:11 PM
 */

#ifndef SYMBOLIMPLICATION_H
#define SYMBOLIMPLICATION_H
#include "include.h"
#include "SymbolPtr.h"
#include "SymbolPosition.h"
#include "SymbolAxis.h"

class SymbolImplication {
public:
    SymbolImplication();
    SymbolImplication(const SymbolImplication& orig);
    virtual ~SymbolImplication();
    
    SymbolPtr other;
    sigt chance;
    SymbolPosition transform;
private:

};

#endif /* SYMBOLIMPLICATION_H */

