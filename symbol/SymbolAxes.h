/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolAxes.h
 * Author: galen
 *
 * Created on September 9, 2016, 4:50 PM
 */

#ifndef SYMBOLAXES_H
#define SYMBOLAXES_H
#include "include.h"
#include "SymbolAxis.h"

namespace SymbolAxes {
    void Initialize();
    void AddAxis(SymbolAxis *axis);
    void Cleanup();
};

#endif /* SYMBOLAXES_H */

