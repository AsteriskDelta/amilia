/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SymbolAssoc.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 8:02 PM
 */

#include "SymbolAssociation.h"

SymbolAssociation::SymbolAssociation()  : center(), radiiWeights(), implications() {
}

SymbolAssociation::SymbolAssociation(const SymbolAssociation& orig)  : center(orig.center), radiiWeights(orig.radiiWeights), implications(orig.implications) {
}

SymbolAssociation::~SymbolAssociation() {
}

float SymbolAssociation::getWeight(const SymbolPosition& p) const {
    float ret = ((p - center) * radiiWeights).magnitude();
    return ret;
}

void SymbolAssociation::associate(SymbolPtr sym, const SymbolPosition &trans, sigt weight) {
    
}