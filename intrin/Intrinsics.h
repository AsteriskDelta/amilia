/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Intrinsics.h
 * Author: galen
 *
 * Created on September 9, 2016, 6:22 PM
 */

#ifndef INTRINSICS_H
#define INTRINSICS_H
#include "include.h"
#include "symbol/AllSymbol.h"
#include "temporal/Time.h"

namespace Intrinsics {
    extern Time *time;
    
    void Initialize();
    void Setup();
    void SetupTemporal();
    
    void newInstanceHook(SymbolInstance *instance);
};

#endif /* INTRINSICS_H */

