/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Intrinsics.cpp
 * Author: galen
 * 
 * Created on September 9, 2016, 6:22 PM
 */

#include "Intrinsics.h"
#include "symbol/AllSymbol.h"

namespace Intrinsics {
  void Initialize() {
      
  }  
  
  void Setup() {
      SetupTemporal();
  }
  
  Intrinsics::Time *time = nullptr;
  void SetupTemporal() {
      if(time == nullptr) {
          time = new Intrinsics::Time();
          SymbolAxes::AddAxis(time);
      }
  }
  
  void newInstanceHook(SymbolInstance *instance) {
      instance->position.setAxis(time, time->current());
  }
};
