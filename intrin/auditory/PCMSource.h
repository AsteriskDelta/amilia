/* 
 * File:   PCMSource.h
 * Author: Galen Swain
 *
 * Created on September 7, 2015, 10:00 AM
 */

#ifndef PCMSOURCE_H
#define	PCMSOURCE_H
#include <AmiliaInt.h>
#include <vector>
#include <atomic>

#define PCMSRC_MAX_OCTAVES 13
typedef Int32 pcm_t;

class PCMSource {
public:
    struct Fragment {
        Time time;
        Uint32 nsSpanned;
        Uint32 rate;
        Uint16 samples;
        Uint8 octaveCount;
        Flags<Uint8> flags;
        pcm_t* octaves[PCMSRC_MAX_OCTAVES];
    };
    
    PCMSource();
    PCMSource(const PCMSource& orig);
    virtual ~PCMSource();
    
    struct{
        Uint32 min, max;
    } freq;
    Uint16 fragmentSize;
    std::vector<Fragment> fragBuffer;
    Uint16 fragbuffHead, fragbuffTail;
    
    inline pcm_t sample(Time time, Uint32 reqRate);
    inline Uint32 octaveRate(Uint8 octave);
    
    inline bool hasNewFragment();
    inline void clearNewFragment();
private:
    std::atomic<bool> newFrag;
};

inline pcm_t PCMSource::sample(Time time, Uint32 reqRate) {
    //Fragment* 
}

inline bool PCMSource::hasNewFragment() {
    return newFrag;
}
inline void PCMSource::clearNewFragment() {
    newFrag = false;
}

#endif	/* PCMSOURCE_H */

