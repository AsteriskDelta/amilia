/* 
 * File:   SpectralSample.h
 * Author: Galen Swain
 *
 * Created on September 12, 2015, 2:26 PM
 */

#ifndef SPECTRALSAMPLE_H
#define	SPECTRALSAMPLE_H

class SpectralSample {
public:
    SpectralSample();
    SpectralSample(const SpectralSample& orig);
    virtual ~SpectralSample();
    
    double sample(double hz);
private:

};

#endif	/* SPECTRALSAMPLE_H */

