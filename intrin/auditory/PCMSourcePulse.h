/* 
 * File:   PCMSourcePulse.h
 * Author: Galen Swain
 *
 * Created on September 11, 2015, 7:26 PM
 */

#ifndef PCMSOURCEPULSE_H
#define	PCMSOURCEPULSE_H

class PCMSourcePulse {
public:
    PCMSourcePulse();
    PCMSourcePulse(const PCMSourcePulse& orig);
    virtual ~PCMSourcePulse();
private:

};

#endif	/* PCMSOURCEPULSE_H */

