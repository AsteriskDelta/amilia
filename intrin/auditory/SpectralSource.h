/* 
 * File:   SpectralSource.h
 * Author: Galen Swain
 *
 * Created on September 11, 2015, 7:26 PM
 */

#ifndef SPECTRALSOURCE_H
#define	SPECTRALSOURCE_H
#include "PCMSource.h"

class SpectralSource {
public:
    struct WindowCoeff {
        float pwr;//        = 1.f
        float twoWidth;//   = 2.f
        float modFreq;//    = 1.f
        float modScale;//   = 0.f
    };
    struct Fragment {
        
    };
    
    SpectralSource(PCMSource* pcm);
    SpectralSource(const SpectralSource& orig);
    virtual ~SpectralSource();
    
    PCMSource* source;
    struct {
        Uint32 min, max;
    } freq;
private:
    inline float windowFunc(float t, const WindowCoeff& coeff) {
        return 0.5f * (1.f + cos(2.f*float(M_PI)* pow(abs(t), coeff.pwr) / coeff.twoWidth));//  * (1 - Abs[Sin[Pi*x*d]]*b)
    }
    void setSource(PCMSource* pcm);
};

#endif	/* SPECTRALSOURCE_H */

