/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Time.h
 * Author: galen
 *
 * Created on September 9, 2016, 6:22 PM
 */

#ifndef INTRIN_TIME_H
#define INTRIN_TIME_H
#include "include.h"
#include "symbol/SymbolAxis.h"
namespace Intrinsics {
class Time : public SymbolAxis {
public:
    Time();
    virtual ~Time();
    
    float currentTime;
    inline float current() const {
        return currentTime;
    }
private:

};
};

#endif /* TIME_H */

